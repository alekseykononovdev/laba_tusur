package postgres

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"log"
	"to-do/interanal/model"
)

type DSNConfig struct {
	Host     string
	User     string
	Password string
	NameDB   string
	Port     string
	SslMode  string
}

func Init(cfg DSNConfig) *gorm.DB {
	dbDsn := fmt.Sprintf("host=%s "+
		"user=%s "+
		"password=%s "+
		"dbname=%s "+
		"port=%s "+
		"sslmode=%s ", cfg.Host, cfg.User, cfg.Password, cfg.NameDB, cfg.Port, cfg.SslMode)

	db, errConnection := gorm.Open(postgres.New(postgres.Config{DSN: dbDsn}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   "todo.", // schema name
			SingularTable: false,
		},
		Logger: logger.Default.LogMode(logger.Silent),
	})
	if errConnection != nil {
		log.Fatalln(errConnection)
	}

	errMigration := db.AutoMigrate(
		&model.ToDo{},
	)
	if errMigration != nil {
		log.Fatalln(errMigration)
	}

	return db
}
