package postgres

import (
	"gorm.io/gorm"
	"to-do/interanal/model"
)

type ToDoRepository struct {
	db *gorm.DB
}

func (t ToDoRepository) CreateToDo(todo model.ToDo) error {
	return t.db.Create(&todo).Error

}

func (t ToDoRepository) FindAllToDo() ([]model.ToDo, error) {
	var todo []model.ToDo
	err := t.db.Find(&todo).Error
	return todo, err
}

func (t ToDoRepository) FindByToDoID(todoId uint) (model.ToDo, error) {
	var todo model.ToDo
	err := t.db.Where("id = ?", todoId).Find(&todo).Error
	return todo, err
}

func (t ToDoRepository) UpdateToDoByID(todo model.ToDo) error {
	return t.db.Save(&todo).Error
}

func (t ToDoRepository) DeleteById(todoId uint) error {
	return t.db.Delete(&model.ToDo{}, todoId).Error
}

func NewToDoRepository(db *gorm.DB) *ToDoRepository {
	return &ToDoRepository{db: db}
}
