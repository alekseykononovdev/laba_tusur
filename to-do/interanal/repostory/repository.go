package repostory

import (
	"gorm.io/gorm"
	"to-do/interanal/model"
	"to-do/interanal/repostory/postgres"
)

type InterfaceToDoRepository interface {
	CreateToDo(todo model.ToDo) error
	FindAllToDo() ([]model.ToDo, error)
	FindByToDoID(todoId uint) (model.ToDo, error)
	UpdateToDoByID(todo model.ToDo) error
	DeleteById(todoId uint) error
}

type Repository struct {
	InterfaceToDoRepository
}

func NewRepository(db *gorm.DB) *Repository {
	return &Repository{
		InterfaceToDoRepository: postgres.NewToDoRepository(db),
	}
}
