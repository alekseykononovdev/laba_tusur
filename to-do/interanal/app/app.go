package app

import (
	"fmt"
	"github.com/spf13/viper"
	"log"
	"to-do/interanal/handler"
	"to-do/interanal/repostory"
	"to-do/interanal/service"
	"to-do/pkg/database/postgres"
	"to-do/pkg/server"
)

type DBConfig struct {
	Host     string `mapstructure:"host"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
	Dbname   string `mapstructure:"dbname"`
	Port     string `mapstructure:"port"`
	SslMode  string `mapstructure:"sslmode"`
}

type Config struct {
	Port     string   `mapstructure:"port"`
	Database DBConfig `mapstructure:"database"`
}

func Run(configPath string) {

	viper.SetConfigFile(configPath)
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err.Error())
		return
	}

	var config Config
	if err := viper.Unmarshal(&config); err != nil {
		fmt.Println(err)
		return
	}

	dbConnection := postgres.Init(postgres.DSNConfig{
		Host:     config.Database.Host,
		User:     config.Database.User,
		Password: config.Database.Password,
		NameDB:   config.Database.Dbname,
		Port:     config.Database.Port,
		SslMode:  config.Database.SslMode,
	})

	repositories := repostory.NewRepository(dbConnection)
	services := service.NewService(repositories)
	handlers := handler.NewHandler(services)

	srv := new(server.Server)
	if err := srv.Run(config.Port, handlers.InitRoutes()); err != nil {
		log.Fatalf("Start up application error while running http server: %s", err.Error())
	}

}
