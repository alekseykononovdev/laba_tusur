package service

import (
	"to-do/interanal/dto"
	"to-do/interanal/model"
	"to-do/interanal/repostory"
)

type ToDoService struct {
	toDoRepository repostory.InterfaceToDoRepository
}

func (t ToDoService) CreateToDo(todoDto dto.ToDoDTO) error {
	todo := model.ToDo{
		Content: todoDto.Content,
		IsDone:  todoDto.IsDone,
	}

	return t.toDoRepository.CreateToDo(todo)
}

func (t ToDoService) FindAllToDo() ([]dto.ToDoDTO, error) {
	todoList, err := t.toDoRepository.FindAllToDo()
	if err != nil {
		return []dto.ToDoDTO{}, err
	}
	var todoDto []dto.ToDoDTO
	for _, td := range todoList {
		todoDto = append(todoDto, dto.ToDoDTO{
			ID:      td.ID,
			Content: td.Content,
			IsDone:  td.IsDone,
		})
	}

	return todoDto, err
}

func (t ToDoService) FindByToDoByID(todoId uint) (dto.ToDoDTO, error) {
	todo, err := t.toDoRepository.FindByToDoID(todoId)
	if err != nil {
		return dto.ToDoDTO{}, err
	}
	return dto.ToDoDTO{
		ID:      todo.ID,
		Content: todo.Content,
		IsDone:  todo.IsDone,
	}, err
}

func (t ToDoService) UpdateToDoByID(todoId uint, content string, isDone bool) error {
	todo, err := t.toDoRepository.FindByToDoID(todoId)
	if err != nil {
		return err
	}
	todo.Content = content
	todo.IsDone = isDone
	return t.toDoRepository.UpdateToDoByID(todo)

}

func (t ToDoService) DeleteById(todoId uint) error {
	return t.toDoRepository.DeleteById(todoId)
}

func NewToDoService(toDoRepository repostory.InterfaceToDoRepository) *ToDoService {
	return &ToDoService{toDoRepository: toDoRepository}
}
