package service

import (
	"to-do/interanal/dto"
	"to-do/interanal/repostory"
)

type InterfaceToDoService interface {
	CreateToDo(todo dto.ToDoDTO) error
	FindAllToDo() ([]dto.ToDoDTO, error)
	FindByToDoByID(todoId uint) (dto.ToDoDTO, error)
	UpdateToDoByID(todoId uint, content string, isDone bool) error
	DeleteById(todoId uint) error
}

type Service struct {
	InterfaceToDoService
}

func NewService(repository *repostory.Repository) *Service {
	return &Service{
		InterfaceToDoService: NewToDoService(repository.InterfaceToDoRepository),
	}
}
