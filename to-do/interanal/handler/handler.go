package handler

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"time"
	"to-do/interanal/service"
)

type Handler struct {
	service *service.Service
}

func NewHandler(service *service.Service) *Handler {
	return &Handler{service: service}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()
	cors.Default()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	router.GET("/ping", h.Pong)
	api := router.Group("/api")
	{
		todo := api.Group("/todo")
		{
			todo.GET("/", h.GetToDoList)
			todo.GET("/:id/", h.GetToDoById)
			todo.POST("/", h.CreateToDo)
			todo.DELETE("/:id/", h.DeleteById)
			todo.PATCH("/:id/", h.UpdateById)
		}
	}
	return router
}
