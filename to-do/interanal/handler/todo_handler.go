package handler

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"to-do/interanal/dto"
)

func (h *Handler) GetToDoList(c *gin.Context) {
	todoList, err := h.service.FindAllToDo()
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
		}
		return
	}
	c.JSON(200, todoList)
}

func (h *Handler) GetToDoById(c *gin.Context) {
	todoId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		}
		return
	}
	todo, err := h.service.FindByToDoByID(uint(todoId))
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
		}
		return
	}
	c.JSON(200, todo)
}

func (h *Handler) CreateToDo(c *gin.Context) {
	var todoDTO *dto.ToDoDTO
	err := json.NewDecoder(c.Request.Body).Decode(&todoDTO)
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		}
		return
	}
	err = h.service.CreateToDo(*todoDTO)
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
		}
		return
	}
	c.String(200, "done")
}

func (h *Handler) DeleteById(c *gin.Context) {
	todoId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		}
		return
	}

	err = h.service.DeleteById(uint(todoId))

	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
		}
		return
	}
	c.Status(200)
}

func (h *Handler) UpdateById(c *gin.Context) {
	todoId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		}
		return
	}

	var todoDTO *dto.ToDoDTO
	err = json.NewDecoder(c.Request.Body).Decode(&todoDTO)
	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		}
		return
	}

	err = h.service.UpdateToDoByID(uint(todoId), todoDTO.Content, todoDTO.IsDone)

	if err != nil {
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
		}
		return
	}
	c.Status(200)
}
