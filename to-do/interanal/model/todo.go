package model

type ToDo struct {
	ID      uint `json:"id" gorm:"primary"`
	Content string
	IsDone  bool
}

func (td *ToDo) TableName() string {
	return "todo.todo"
}
