package dto

type ToDoDTO struct {
	ID      uint   `json:"ID"`
	Content string `json:"content"`
	IsDone  bool   `json:"isDone"`
}
