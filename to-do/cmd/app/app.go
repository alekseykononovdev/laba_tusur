package main

import "to-do/interanal/app"

const testConfigPath = "configs/config-test.yaml"

func main() {
	app.Run(testConfigPath)
}
