# Collections-service Microservice


# port app: 
8002

# database:
    host: localhost
    user: postgres
    password: password
    dbname: laba
    port: 5432
    sslmode: allow

# API

    GET    /ping  
    POST   /api/todo/ 
    GET    /api/todo/            
    GET    /api/todo/:id/         
    DELETE /api/todo/:id/        
    PATCH  /api/todo/:id/  

# Examples
![img_1.png](img_1.png)

![img.png](img.png)

![img_3.png](img_3.png)

![img_4.png](img_4.png)

![img_5.png](img_5.png)

![img_6.png](img_6.png)

#### Result

![img_7.png](img_7.png)
